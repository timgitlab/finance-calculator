**GUI to calculate loan term given:**

<img src="/image.png" width="700" />


- Property value (and an interest schedule based on this).
- Principal.
- Repayment rate / frequency.
- Interest rate increases/decreases.
- Lump sum payments/withdrawals.

More stuff TBD.
