import sys
import math

from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit, QLabel, QHBoxLayout, QVBoxLayout, QGridLayout, QPushButton, QMainWindow, QAction
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib

def calculation(self, value, loan, frequency, repayments, events):
    interestRate = {
        0.8: 2.29,
        0.7: 2.24,
        0.6: 2.19
    }
    repaymentFreq = frequency
    LVR = 1
    d = 0
    year = []
    outstanding = []
    interest = 0
    while loan > 0:
        LVR = math.ceil(loan / value * 10)/10
        for key in interestRate:
            if LVR < key:
                LVR = key
        currentRate = interestRate.get(LVR)
        if currentRate == None:
            duration = 'LVR rules violated'
            return year, outstanding, duration
        d += 1
        interest += loan * currentRate / 100 / 365
        if d % repaymentFreq == 0:
            loan += interest - repayments
            interest = 0
        year.append(d / 365)
        outstanding.append(loan)

        #do the more complex fields here
        if d in events['days']:
            #find the location in the days array
            location = events['days'].index(d)
            #remove the lump sum
            loan -= int(events['lumps'][location])
            #add the interest rate increase
            for key in interestRate:
                interestRate[key] += float(events['rates'][location])

        duration = d / 365
    return year, outstanding, duration

#class MyApp(QWidget):
class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowTitle('Home Loan Schedule Comparison Program')
        self.window_width, self.window_height = 1200, 900
        self.setMinimumSize(self.window_width, self.window_height)
        self.initMenu()
        self.initGrid()

    def initGrid(self):
        #prep work for setting layout inside application
        wid = QWidget(self)
        self.setCentralWidget(wid)
        #assign grid app layout
        layout = QGridLayout()
        wid.setLayout(layout)

        #-----------------------------------
        #(row, col, row span, col span)
        #------------------------------------
        y_labels = 0
        y_fields = 1
        y_fields_alt = 2
        y_calc = 3
        y_graph = 4
        y_labels_complex = 6
        y_fields_complex = 7

        #add field labels to GUI
        self.label_value = QLabel()
        self.label_loan = QLabel()
        self.label_freq = QLabel()
        self.label_amount = QLabel()

        self.label_value.setText("House value")
        self.label_loan.setText("Starting principal")
        self.label_freq.setText("Repayment frequency")
        self.label_amount.setText("Repayment amount")

        layout.addWidget(self.label_value, y_labels, 0)
        layout.addWidget(self.label_loan, y_labels, 1)
        layout.addWidget(self.label_freq, y_labels, 2)
        layout.addWidget(self.label_amount, y_labels, 3)

        #add the house value input
        self.input_value = QLineEdit()
        self.input_value.setText('1000000')
        layout.addWidget(self.input_value, y_fields, 0)

        #alternate data label
        self.label_alternate = QLabel()
        self.label_alternate.setText("Alternate calculation -->")
        layout.addWidget(self.label_alternate, y_fields_alt, 0)

        #automate the alternate value fields
        self.basicDictionary = {'loan': 800000,
                                'freq': 7,
                                'amount': round(72000 / 52)
                                }
        #iterate over the x-position fields
        x_fields = 1
        for item in self.basicDictionary:
            varName = 'input_' + item
            #add the name for the alternate fields
            varNameAlt = 'input_' + item + '_alt'

            globals()[varName] = QLineEdit()
            globals()[varNameAlt] = QLineEdit()

            globals()[varName].setText(str(self.basicDictionary[item]))

            layout.addWidget(globals()[varName], y_fields, x_fields)
            layout.addWidget(globals()[varNameAlt], y_fields_alt, x_fields)

            x_fields += 1

        #add the re-calculate button
        self.button = QPushButton('Recalculate schedule', self)
        self.button.setToolTip('Enter amount ($/freq.)')
        self.button.clicked.connect(self.on_click)
        layout.addWidget(self.button, y_calc, 0, 1, 3)

        #add a duration print window
        self.label_duration = QLabel()
        layout.addWidget(self.label_duration, y_calc, 3)

        #add the graph field
        self.canvas = FigureCanvas(plt.Figure(figsize=(10, 6)))
        layout.addWidget(self.canvas, y_graph, 0, 2, 4)

        #add the more complex entries labels
        self.label_month = QLabel()
        self.label_month.setText("Month (of event)")
        self.label_rate = QLabel()
        self.label_rate.setText("Rate change (%)")
        self.label_lump = QLabel()
        self.label_lump.setText("Lump Sum ($)")

        layout.addWidget(self.label_month, y_labels_complex, 0)
        layout.addWidget(self.label_rate, y_labels_complex, 1)
        layout.addWidget(self.label_lump, y_labels_complex, 2)

        #add the more complex fields using an automated method
        self.complexDictionary = {'line1': {'month': 'input_month_1', 'rate': 'input_rate_1', 'lump': 'input_lump_1'},
                                'line2': {'month': 'input_month_2', 'rate': 'input_rate_2', 'lump': 'input_lump_2'},
                                'line3': {'month': 'input_month_3', 'rate': 'input_rate_3', 'lump': 'input_lump_3'},
                                'line4': {'month': 'input_month_4', 'rate': 'input_rate_4', 'lump': 'input_lump_4'}
                                }

        for line in self.complexDictionary:
            varMonth = self.complexDictionary[line]['month']
            varRate = self.complexDictionary[line]['rate']
            varLump = self.complexDictionary[line]['lump']

            globals()[varMonth] = QLineEdit()
            globals()[varRate] = QLineEdit()
            globals()[varLump] = QLineEdit()

            layout.addWidget(globals()[varMonth], y_fields_complex, 0)
            layout.addWidget(globals()[varRate], y_fields_complex, 1)
            layout.addWidget(globals()[varLump], y_fields_complex, 2)

            y_fields_complex += 1

        #call the grpahing function
        self.insert_ax()

    def initMenu(self):

        # Create new action
        newAction = QAction(QIcon('new.png'), '&New', self)
        newAction.setShortcut('Ctrl+N')
        newAction.setStatusTip('New document')
        newAction.triggered.connect(self.newCall)

        # Create new action
        openAction = QAction(QIcon('open.png'), '&Open', self)
        openAction.setShortcut('Ctrl+O')
        openAction.setStatusTip('Open document')
        openAction.triggered.connect(self.openCall)

        # Create exit action
        exitAction = QAction(QIcon('exit.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.exitCall)

        # Create menu bar and add action
        menuBar = self.menuBar()
        fileMenu = menuBar.addMenu('&File')
        fileMenu.addAction(newAction)
        fileMenu.addAction(openAction)
        fileMenu.addAction(exitAction)

        self.show()

    def openCall(self):
        print('Open')

    def newCall(self):
        self.initGrid()

    def exitCall(self):
        self.close()

    def clickMethod(self):
        print('PyQt')

    def collect_data(self):
        #accept the basic data
        houseValue = int(self.input_value.text())
        for item in self.basicDictionary:
            varName = 'input_' + item
            varNameAlt = 'input_' + item + '_alt'
            locals()[item] = int(globals()[varName].text())
            if globals()[varNameAlt].text() == '':
                locals()[item + '_alt'] = ''
            else:
                locals()[item + '_alt'] = int(globals()[varNameAlt].text())

        #accept more complex (bottom line) information from GUI and list
        #dynamically sized arrays
        Days = []
        Rates = []
        Lumps = []
        for line in self.complexDictionary:
            #turn the dictionary into variable names
            varMonth = self.complexDictionary[line]['month']
            varRate = self.complexDictionary[line]['rate']
            varLump = self.complexDictionary[line]['lump']
            #cycle through dynamically made variables and assign values from fields if month =! empty
            if globals()[varMonth].text() != '':
                days = round(int(globals()[varMonth].text()) * 365 / 12)
                Days.append(days)
                if globals()[varRate].text() == '':
                    Rates.append(0)
                else:
                    Rates.append(globals()[varRate].text())
                if globals()[varLump].text() == '':
                    Lumps.append(0)
                else:
                    Lumps.append(globals()[varLump].text())
        events = {'days': Days, 'rates': Rates, 'lumps': Lumps}

        returnData = {'value': houseValue, 'loan': locals()['loan'], 'freq': locals()['freq'], 'amt': locals()['amount'], 'events': events,
                      'loan_alt': locals()['loan_alt'], 'freq_alt': locals()['freq_alt'], 'amt_alt': locals()['amount_alt']}
        return returnData

    def insert_ax(self):
        font = {
            'weight': 'normal',
            'size': 16
        }
        matplotlib.rc('font', **font)
        #call the function that gathers data
        returnData = self.collect_data()
        #call the function that calculates the repayment curve
        years, outstanding, duration = calculation(self, value=returnData['value'],
                loan=returnData['loan'], repayments=returnData['amt'], frequency=returnData['freq'], events=returnData['events'])
        #set the label field to duration
        self.label_duration.setText('Pay off in %.1f years' % duration)
        #graph the plots
        self.ax = self.canvas.figure.subplots()
        self.update_chart(years = years, outstanding = outstanding, years_alt = [], outstanding_alt = [])

    def update_chart(self, years, outstanding, years_alt, outstanding_alt):
        self.ax.clear()
        self.ax.plot(years, outstanding)
        self.ax.plot(years_alt, outstanding_alt)
        self.ax.set_title('Home Loan Schedule')
        self.ax.set_xlabel('Years')
        self.ax.set_ylabel('Outstanding ($)')
        self.ax.grid()
        self.ax.set_xlim(0, None)
        self.ax.set_ylim(0, None)
        if years_alt != []:
            self.ax.legend(['Base Calculation', 'Alternate Calculation'], fontsize=12)
        else:
            self.ax.legend().remove()
        self.canvas.draw()

    @pyqtSlot()
    def on_click(self):
        #run new calculations and plot
        returnData = self.collect_data()
        years, outstanding, duration = calculation(self, value=returnData['value'],
                loan=returnData['loan'], repayments=returnData['amt'], frequency=returnData['freq'], events={'days': [], 'rates': [], 'lumps': []})
        #check that alternate data exists before handing to calculation method
        if returnData['loan_alt'] != '' and returnData['amt_alt'] != '' and returnData['freq_alt'] != '':
            years_alt, outstanding_alt, duration_alt = calculation(self, value=returnData['value'],
                loan=returnData['loan_alt'], repayments=returnData['amt_alt'], frequency=returnData['freq_alt'], events=returnData['events'])
            self.update_chart(years=years, outstanding=outstanding, years_alt=years_alt, outstanding_alt=outstanding_alt)
            if duration_alt == 'LVR rules violated':
                self.label_duration.setText('%.1f years / LVR exceeded' % duration)
            else:
                self.label_duration.setText('Pay off in %.1f / %.1f years' % (duration, duration_alt))
        else:
            self.update_chart(years=years, outstanding=outstanding, years_alt=[], outstanding_alt=[])
            if duration == 'LVR rules violated':
                self.label_duration.setText(duration)
            else:
                self.label_duration.setText('Pay off in %.1f years' % duration)

if __name__ == '__main__':
    # don't auto scale when drag app to a different monitor.
    # QApplication.setAttribute(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)

    app = QApplication(sys.argv)
    myWindow = MainWindow()
    myWindow.show()
    # myApp = MyApp()
    # myApp.show()
    app.setStyleSheet('''
        QWidget {
            font-size: 20px;
        }
    ''')

    try:
        sys.exit(app.exec_())
    except SystemExit:
        print('Closing Window...')
